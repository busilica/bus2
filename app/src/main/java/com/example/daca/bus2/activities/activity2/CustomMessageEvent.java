package com.example.daca.bus2.activities.activity2;

import java.util.ArrayList;

/**
 * Created by Daca on 25.01.2017..
 */
public class CustomMessageEvent {
    private String customStringMessage;
    private ArrayList<String> customMessage1;
    private ArrayList<String> customMessage2;
    private ArrayList<String> customMessage3;

    public ArrayList<String> getCustomMessage1() {
        return customMessage1;
    }

    public void setCustomMessage1(ArrayList<String> customMessage1) {
        this.customMessage1 = customMessage1;
    }

    public ArrayList<String> getCustomMessage2() {
        return customMessage2;
    }

    public void setCustomMessage2(ArrayList<String> customMessage2) {
        this.customMessage2 = customMessage2;
    }

    public ArrayList<String> getCustomMessage3() {
        return customMessage3;
    }

    public void setCustomMessage3(ArrayList<String> customMessage3) {
        this.customMessage3 = customMessage3;
    }

    public String getCustomStringMessage() {
        return customStringMessage;
    }

    public void setCustomStringMessage(String customStringMessage) {
        this.customStringMessage = customStringMessage;
    }
}
