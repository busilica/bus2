package com.example.daca.bus2.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.bus2.R;

/**
 * Created by Daca on 19.01.2017..
 */

class Holder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView tv;


    Holder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.imageView_fragment_radni_dan);
        tv = (TextView) itemView.findViewById(R.id.textView_fragment_radni_dan);

    }
}
