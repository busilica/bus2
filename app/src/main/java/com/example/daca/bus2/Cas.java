package com.example.daca.bus2;

import java.util.ArrayList;

/**
 * Created by Daca on 19.01.2017..
 */

public class Cas {



    String cas;
    String casSubota;
    String casNedelja;
    int imageView;

    public Cas(String cas, String casSubota, String casNedelja, int imageView) {
        this.cas = cas;
        this.casSubota = casSubota;
        this.casNedelja = casNedelja;
        this.imageView = imageView;
    }

  //  public static ArrayList<Cas> arrayCasList = new ArrayList<>();



    public static void generateData(){
        ArrayList<Cas> arrayList = new ArrayList<>();
        arrayList.add(new Cas("30**, 47**, 50", "01** 45** 50", "00**, 36**",  R.drawable.b4));
        arrayList.add(new Cas("02**, 05, 13**, 21, 31**, 37, 53", "10, 40", "05**, 10, 42", R.drawable.b5));
        arrayList.add(new Cas("00**, 09, 12, 28, 31**, 37, 47, 56*", "15, 46", "14, 46",  R.drawable.b6));
        arrayList.add(new Cas("06, 15*, 25, 35*, 44, 53", "32*, 40**", "18, 40**, 50", R.drawable.b7));
        arrayList.add(new Cas("04*, 16, 28*, 40, 52*", "00, 25*, 50", "22, 46", R.drawable.b8));
        arrayList.add(new Cas("04 16*, 28, 40*, 52", "15*, 40", "10, 34, 58", R.drawable.b9));
        arrayList.add(new Cas("04*, 16, 28*, 40, 52*", "05*, 30, 55*", "22, 46", R.drawable.b10));
        arrayList.add(new Cas("04, 16*, 28, 40, 52", "18*, 41, 42**", "10, 34, 58", R.drawable.b11));
        arrayList.add(new Cas("04, 16, 26, 35, 45, 54", "06, 23, 25**, 37, 51", "22, 46", R.drawable.b12));
        arrayList.add(new Cas("04, 13, 22, 32, 41, 51", "05, 19, 33 47", "10, 34, 58", R.drawable.b13));
        arrayList.add(new Cas("00, 10, 19, 29, 38, 48, 57", "01, 15, 29, 43, 57", "22, 46", R.drawable.b14));
        arrayList.add(new Cas("07, 16, 26, 35, 45, 54*", "22, 47", "10, 34, 58", R.drawable.b15));
        arrayList.add(new Cas("04, 14*, 26, 38*, 50", "2, 37", "30", R.drawable.b16));
        arrayList.add(new Cas("02*, 14, 26*, 38, 50*", "02, 27, 52", "02, 48", R.drawable.b17));
        arrayList.add(new Cas("02, 14*, 26, 38*, 50", "17, 42", "40", R.drawable.b18));
        arrayList.add(new Cas("02*, 14, 26*, 38, 50*", "07, 32, 57", "27", R.drawable.b19));
        arrayList.add(new Cas("02, 25, 49", "19, 51", "14", R.drawable.b20));
        arrayList.add(new Cas("13, 37", "23, 55", "01, 48", R.drawable.b21));
        arrayList.add(new Cas("01, 25, 49", "27, 59", "35", R.drawable.b22));
        arrayList.add(new Cas("13, 37", "31", "22", R.drawable.b23));
        arrayList.add(new Cas("01, 25", "03, 35", "5", R.drawable.b0));
        arrayList.add(new Cas("45***", "45", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("15", "", "", R.drawable.b4));
        //arrayCasList = arrayList;
    }

    public static void generateData2(){
        ArrayList<Cas> arrayList = new ArrayList<>();
        arrayList.add(new Cas("30**, 47**, 50", "01** 45** 50", "00**, 36**",  R.drawable.b4));
        arrayList.add(new Cas("02**, 05, 13**, 21, 31**, 37, 53", "10, 40", "05**, 10, 42", R.drawable.b5));
        arrayList.add(new Cas("00**, 09, 12, 28, 31**, 37, 47, 56*", "15, 46", "14, 46",  R.drawable.b6));
        arrayList.add(new Cas("06, 15*, 25, 35*, 44, 53", "32*, 40**", "18, 40**, 50", R.drawable.b7));
        arrayList.add(new Cas("04*, 16, 28*, 40, 52*", "00, 25*, 50", "22, 46", R.drawable.b8));
        arrayList.add(new Cas("04 16*, 28, 40*, 52", "15*, 40", "10, 34, 58", R.drawable.b9));
        arrayList.add(new Cas("04*, 16, 28*, 40, 52*", "05*, 30, 55*", "22, 46", R.drawable.b10));
        arrayList.add(new Cas("04, 16*, 28, 40, 52", "18*, 41, 42**", "10, 34, 58", R.drawable.b11));
        arrayList.add(new Cas("04, 16, 26, 35, 45, 54", "06, 23, 25**, 37, 51", "22, 46", R.drawable.b12));
        arrayList.add(new Cas("04, 13, 22, 32, 41, 51", "05, 19, 33 47", "10, 34, 58", R.drawable.b13));
        arrayList.add(new Cas("00, 10, 19, 29, 38, 48, 57", "01, 15, 29, 43, 57", "22, 46", R.drawable.b14));
        arrayList.add(new Cas("07, 16, 26, 35, 45, 54*", "22, 47", "10, 34, 58", R.drawable.b15));
        arrayList.add(new Cas("04, 14*, 26, 38*, 50", "2, 37", "30", R.drawable.b16));
        arrayList.add(new Cas("02*, 14, 26*, 38, 50*", "02, 27, 52", "02, 48", R.drawable.b17));
        arrayList.add(new Cas("02, 14*, 26, 38*, 50", "17, 42", "40", R.drawable.b18));
        arrayList.add(new Cas("02*, 14, 26*, 38, 50*", "07, 32, 57", "27", R.drawable.b19));
        arrayList.add(new Cas("02, 25, 49", "19, 51", "14", R.drawable.b20));
        arrayList.add(new Cas("13, 37", "23, 55", "01, 48", R.drawable.b21));
        arrayList.add(new Cas("01, 25, 49", "27, 59", "35", R.drawable.b22));
        arrayList.add(new Cas("13, 37", "31", "22", R.drawable.b23));
        arrayList.add(new Cas("01, 25", "03, 35", "5", R.drawable.b0));
        arrayList.add(new Cas("45***", "45", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("15", "", "", R.drawable.b4));
        //arrayCasList = arrayList;
    }

   /* public static void BubanjDonjaVrezina(){
        ArrayList<Cas> arrayList = new ArrayList<>();
        arrayList.add(new Cas("30, 45*", "25*, 40, 55", "",  R.drawable.b4));
        arrayList.add(new Cas("00,15*,30,36,42*,48,54", "10*, 25, 40, 55*", "30*,42,54", R.drawable.b5));
        arrayList.add(new Cas("00*,06*,12*,24*,30*,36*,42*,48*,54*", "10*, 25*, 40*, 50", "06*,18,30,42*,54",  R.drawable.b6));
        arrayList.add(new Cas("00*,06*,12*,18,24,30,36,42,48,54*", "00, 10, 20, 30*, 40, 50", "06,18*,30,42,54*", R.drawable.b7));
        arrayList.add(new Cas("00,10,20,30*,40,50", "00, 10, 20*, 30, 40, 50", "06,18,30*,42,54", R.drawable.b8));
        arrayList.add(new Cas("00,10,20*,30,40,50", "00, 10*, 20, 30, 40, 50", "06*,18,30,42*,54", R.drawable.b9));
        arrayList.add(new Cas("00,10*,20,30,40,50", "00*, 10, 20, 30, 40, 50*", "06,18*,30,42,54*", R.drawable.b10));
        arrayList.add(new Cas("00*,10*,20,30,36,42*,48,54", "00, 10, 20, 30, 40*, 50", "06,18,30*,42,54", R.drawable.b11));
        arrayList.add(new Cas("00,06*,12*,18,24,30*,36*,42,48,54*", "00, 10, 20, 30*, 40, 50", "06*,18,30,42*,54", R.drawable.b12));
        arrayList.add(new Cas("00,06,12,18*,24,30,36,42*,48,54", "00, 10, 20*, 30, 40, 50", "06,18*,30,42", R.drawable.b13));
        arrayList.add(new Cas("00,06*,12,18,24,30*,36,42,48,54*", "10, 25*, 40, 55", "00*,18,36,54*", R.drawable.b14));
        arrayList.add(new Cas("00,06,12,18*,24*,30,36,42*,48*,54*", "10, 25*, 40, 55*", "12,30,48*", R.drawable.b15));
        arrayList.add(new Cas("00*,06*,12*,20*,26*,36*,46,56", "10*, 25*, 40*, 55", "06,24,42*", R.drawable.b16));
        arrayList.add(new Cas("06,16,26*,36*,46,56", "10, 25*, 40, 55", "00,18,36*,54", R.drawable.b17));
        arrayList.add(new Cas("06,16*,26,36,46,56", "10, 25*, 40, 55", "12,30*,48", R.drawable.b18));
        arrayList.add(new Cas("06*,16,26,36,46,56*, 50*", "10, 25*, 40, 55", "06,24*,48", R.drawable.b19));
        arrayList.add(new Cas("08,20,35*,44,58*", "10, 25*, 40, 55", "13,38*", R.drawable.b20));
        arrayList.add(new Cas("12,26,40,54*", "10, 25*, 40, 55", "03,28,53*", R.drawable.b21));
        arrayList.add(new Cas("10,26,42,58*", "10, 25*, 40, 55", "18,43", R.drawable.b22));
        arrayList.add(new Cas("14,40*", "10, 25*, 40", "08*,33", R.drawable.b23));
        arrayList.add(new Cas("00**", "00**", "", R.drawable.b0));
        arrayList.add(new Cas("00**", "00**", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("", "", "", R.drawable.b3));
        arrayCasList = arrayList;
    }

    public static void generateDataBubanjDonjaVrezina2(){
        ArrayList<Cas> arrayList = new ArrayList<>();
        arrayList.add(new Cas("48*", "47*", "", R.drawable.b4));
        arrayList.add(new Cas("07, 20*, 38, 50*", "02*, 20, 35, 47*", "", R.drawable.b5));
        arrayList.add(new Cas("09,15,18*,27,33,36*,42*,48*", "05,20,32*,47*", "06*,24,36,42*", R.drawable.b6));
        arrayList.add(new Cas("00*,06*,12*,18*,24*,30*,36*,42*,48*,57",	"02*,17*,28,38,48,58",	"00,12,18*,35,48,54*", R.drawable.b7));
        arrayList.add(new Cas("03,09,15,21,27,30*,39,48,58", "05,18,28,38,48,56*",	"12,24,30*,48", R.drawable.b8));
        arrayList.add(new Cas("05*,18,28,38,48,55*", "08,18,28,38,45,58", "00,06*,24,36,42*", R.drawable.b9));
        arrayList.add(new Cas("08,18,28,38,45*,58",	"08,18,28,35*,48,58", "00,12,18*,36,48,54*", R.drawable.b10));
        arrayList.add(new Cas("08,18,28,35*,45*,58", "08,18,25*,38,48,58", "12,24,30*,48", R.drawable.b11));
        arrayList.add(new Cas("08,15,18*,27,33,39,42*,48*,57",	"08,15*,28,38,48,58", "00,06*,24,36,42*", R.drawable.b12));
        arrayList.add(new Cas("03,06*,12*,21,27,30*,39,45,51,54*",	"05*,18,28,38,48,55*", "00,12,18*,36,48,54*", R.drawable.b13));
        arrayList.add(new Cas("03,09,15,18*,27,33,39,42*,51,57",	"08,20,35,50", "12,24,36*", R.drawable.b14));
        arrayList.add(new Cas("03,06*,15,21,27,30*,39,45,51,54*",	"02*,20,35,47*", "00,18,30*,54", R.drawable.b15));
        arrayList.add(new Cas("00*,09,15,18*,24*,30*,36*,42*,48*,56*", "05,20,32*,47*",	"12,24*,48", R.drawable.b16));
        arrayList.add(new Cas("02*,11*,24,34,44,54", "02*,17*,35,50", "06,18*,42", R.drawable.b17));
        arrayList.add(new Cas("01*,11*,24,34,44,51*", "02*,20,35,50", "00,12*,36,54", R.drawable.b18));
        arrayList.add(new Cas("01*,14,24,34,41*,54", "02*,20,35,50", "06*,30,48", R.drawable.b19));
        arrayList.add(new Cas("04,14,24,31*,47,59",	"02*,20,35,50",	"00*,31,56", R.drawable.b20));
        arrayList.add(new Cas("11,23,33*,50", "02*,20,35,50", "15*,46", R.drawable.b21));
        arrayList.add(new Cas("04,18,29*,46", "02*,20,35,50", "11,30*", R.drawable.b22));
        arrayList.add(new Cas("00,16,30*", "02*,20,35,47*", "01,26,45*", R.drawable.b23));
        arrayList.add(new Cas("30**", "08, 30**", "16", R.drawable.b0));
        arrayList.add(new Cas("30**", "30***", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("15", "", "", R.drawable.b3));
        arrayCasList = arrayList;
    }

    public static void generateDataBrziBrodNaseljeRatkoJovic(){
        ArrayList<Cas> arrayList = new ArrayList<>();
        arrayList.add(new Cas("45***", "", "", R.drawable.b4));
        arrayList.add(new Cas("11***,45,","00***, 55","30", R.drawable.b5));
        arrayList.add(new Cas("13,26, 31*, 39,52,","18, 40","00, 30", R.drawable.b6));
        arrayList.add(new Cas("02**, 18,31,44,57,","05, 29, 52","00**, 30", R.drawable.b7));
        arrayList.add(new Cas("00*, 10, 28,46,","16, 39","00, 25, 50", R.drawable.b8));
        arrayList.add(new Cas("04, 16*, 22,40,58,","03, 26, 49","15, 40", R.drawable.b9));
        arrayList.add(new Cas("16, 28*,34,52,","13, 37","5, 30, 55**", R.drawable.b10));
        arrayList.add(new Cas("10,28,46,47*,","00**,18, 35, 53","20, 45", R.drawable.b11));
        arrayList.add(new Cas("04, 17,26**,43,56,","10, 28, 45","10, 35", R.drawable.b12));
        arrayList.add(new Cas("05*,09,20***, 29,46,59,","03, 20**, 38, 55","00, 25**, 50", R.drawable.b13));
        arrayList.add(new Cas("12,24*,25,40,51,","18, 42","15, 40", R.drawable.b14));
        arrayList.add(new Cas("04**,17,30,36*,43,56,","05, 29, 52","05, 30, 55", R.drawable.b15));
        arrayList.add(new Cas("15,35,55,","16, 39","25, 55", R.drawable.b16));
        arrayList.add(new Cas("07*,12,34,52,","03, 26, 50","25, 55", R.drawable.b17));
        arrayList.add(new Cas("10,20*,28,46,","13, 48","25, 55**", R.drawable.b18));
        arrayList.add(new Cas("04,22,40,58**,","23, 58**","25, 55", R.drawable.b19));
        arrayList.add(new Cas("16,35,54,","33","25", R.drawable.b20));
        arrayList.add(new Cas("18***,45**,","08, 43","00, 35", R.drawable.b21));
        arrayList.add(new Cas("10, 45**,","18, 54","10, 45", R.drawable.b22));
        arrayList.add(new Cas("10,35,","30","20", R.drawable.b23));

        arrayList.add(new Cas("30**", "08, 30**", "16", R.drawable.b0));
        arrayList.add(new Cas("30**", "30***", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("15", "", "", R.drawable.b3));
        arrayCasList = arrayList;
    }

    public static void generateDataNaseljeRatkoJovicBrziBrod(){
        ArrayList<Cas> arrayList = new ArrayList<>();
       arrayList.add(new Cas("35",25","50", R.drawable.b4));
        arrayList.add(new Cas("05, 25***,47, 50*, 52***",20, 40***,","50", R.drawable.b4));
        arrayList.add(new Cas("13, 26, 38, 52",09, 30, 54","21, 51", R.drawable.b4));
        arrayList.add(new Cas("05, 12*, 18, 31,44, 57",17, 41","21, 46", R.drawable.b4));
        arrayList.add(new Cas("10, 28, 40*, 46,",04, 28, 51","11, 36", R.drawable.b4));
        arrayList.add(new Cas("04,22, 40,52*,58",14, 38","01**, 26, 51", R.drawable.b4));
        arrayList.add(new Cas("16, 34, 52,",02, 25, 43","16, 41", R.drawable.b4));
        arrayList.add(new Cas("04*,10, 28, 46,",00, 18, 35**, 53","06, 31**, 56", R.drawable.b4));
        arrayList.add(new Cas("04, 16, 23*, 29, 42,53",10, 28, 45,","21, 46", R.drawable.b4));
        arrayList.add(new Cas("08, 21, 34, 42*, 47,",03, 20, 43","11, 36", R.drawable.b4));
        arrayList.add(new Cas("05,, 10***, 26, 39,52,",07, 30, 54","01, 26, 51", R.drawable.b4));
        arrayList.add(new Cas("00* ,05, 18,31, 53",17, 41","16, 41", R.drawable.b4));
        arrayList.add(new Cas("12*, 19, 36,55",04, 28, 51","11, 41**", R.drawable.b4));
        arrayList.add(new Cas("16, 34, 43*,52,",15, 38**","11, 41", R.drawable.b4));
        arrayList.add(new Cas("10, 28, 46,",13, 48","11, 41", R.drawable.b4));
        arrayList.add(new Cas("04, 22, 40, 58",23, 58","11, 41", R.drawable.b4));
        arrayList.add(new Cas("16,39**",33**,","20", R.drawable.b4));
        arrayList.add(new Cas("00, 30,",08, 43","00**, 35", R.drawable.b4));
        arrayList.add(new Cas("05***, 30,55",18, 53","10, 45", R.drawable.b4));
        arrayList.add(new Cas("25", "","20", R.drawable.b4));

        arrayList.add(new Cas("30**", "08, 30**", "16", R.drawable.b0));
        arrayList.add(new Cas("30**", "30***", "", R.drawable.b1));
        arrayList.add(new Cas("", "", "", R.drawable.b2));
        arrayList.add(new Cas("15", "", "", R.drawable.b3));
        arrayCasList = arrayList;
    }*/





    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }



    public String getCasSubota() {
        return casSubota;
    }

    public void setCasSubota(String casSubota) {
        this.casSubota = casSubota;
    }

    public String getCasNedelja() {
        return casNedelja;
    }

    public void setCasNedelja(String casNedelja) {
        this.casNedelja = casNedelja;
    }
}
