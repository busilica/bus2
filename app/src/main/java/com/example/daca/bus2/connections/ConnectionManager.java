package com.example.daca.bus2.connections;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Daca on 20.01.2017..
 */

public class ConnectionManager {

    private static RequestQueue queue;

    //getInstance method will create new Singleton object if it doesn't exist
    //if it does exist it will return it
    public static RequestQueue getInstance(Context context){
        if (queue == null){
            queue = Volley.newRequestQueue(context);
        }
        return queue;
    }
}
