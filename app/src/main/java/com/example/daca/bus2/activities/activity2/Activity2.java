package com.example.daca.bus2.activities.activity2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.daca.bus2.R;

import org.greenrobot.eventbus.EventBus;


public class Activity2 extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    int counter = 2;
    ImageButton arrows;
    ViewPagerAdapter adapter;
    TextView tv1;
    TextView tv2;
    Intent intent = new Intent();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        intent = getIntent();
        tv1 = (TextView) findViewById(R.id.tv1_activity2);
        tv1.setText(intent.getStringExtra("direction1"));
        tv2 = (TextView) findViewById(R.id.tv2_activity2);
        tv2.setText(intent.getStringExtra("direction2"));
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_activity2);
        arrows = (ImageButton) findViewById(R.id.arrows_activity2);
        viewPager = (ViewPager) findViewById(R.id.viewPager_activity2);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), intent, counter);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        arrows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (counter % 2 == 0){
                    tv1.setText(intent.getStringExtra("direction2"));
                    tv2.setText(intent.getStringExtra("direction1"));
                    adapter = new ViewPagerAdapter(getSupportFragmentManager(), intent, counter);
                    viewPager.setAdapter(adapter);
                    counter++;
                }else {
                    tv1.setText(intent.getStringExtra("direction1"));
                    tv2.setText(intent.getStringExtra("direction2"));
                    adapter = new ViewPagerAdapter(getSupportFragmentManager(), intent, counter);
                    viewPager.setAdapter(adapter);
                    counter++;
                }
            }
        });
    }

}
