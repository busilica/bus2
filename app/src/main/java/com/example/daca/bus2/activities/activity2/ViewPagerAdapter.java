package com.example.daca.bus2.activities.activity2;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.daca.bus2.fragments.Fragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Daca on 19.01.2017..
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    Intent intent = new Intent();
    int counter;
    ArrayList<String> radniDan;
    ArrayList<String> subota = new ArrayList<>();
    ArrayList<String> nedelja = new ArrayList<>();


    public ViewPagerAdapter(FragmentManager fm, Intent intent, int counter) {

        super(fm);
        EventBus.getDefault().register(this);
        this.intent = intent;
        this.counter = counter;
    }

    @Subscribe
    public void onEvent(CustomMessageEvent event){
        this.radniDan = event.getCustomMessage1();
        this.subota = event.getCustomMessage2();
        this.nedelja = event.getCustomMessage3();
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        android.support.v4.app.Fragment returnFragment;
        switch (position){
            case 0:
                Fragment fragment0 = new Fragment();
                if (counter % 2 == 0){
                    returnFragment = fragment0.newInstance(radniDan);
                }else {
                    returnFragment = fragment0.newInstance(intent.getStringArrayListExtra("workday2"));
                }
                break;
            case 1:
                Fragment fragment1 = new Fragment();
                if (counter % 2 == 0){
                    returnFragment = fragment1.newInstance(subota);
                }else {
                    returnFragment = fragment1.newInstance(nedelja);
                }
                break;
            case 2:
                Fragment fragment2 = new Fragment();
                if (counter % 2 == 0){
                    returnFragment = fragment2.newInstance(intent.getStringArrayListExtra("sunday1"));
                }else {
                    returnFragment = fragment2.newInstance(intent.getStringArrayListExtra("sunday2"));
                }
                break;
            default:
                returnFragment = null;
                break;
        }
        return returnFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title;
        switch (position){
            case 0:
                title = "RADNI DAN";
                break;
            case 1:
                title = "SUBOTA";
                break;
            case 2:
                title = "NEDELJA";
                break;
            default:
                title = null;
                break;
        }
        return title;
    }



}
