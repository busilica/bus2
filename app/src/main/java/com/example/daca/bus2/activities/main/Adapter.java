package com.example.daca.bus2.activities.main;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import com.example.daca.bus2.entities.Linija;
import com.example.daca.bus2.R;
import com.example.daca.bus2.connections.WebScrapper;

import java.util.ArrayList;

/**
 * Created by Daca on 18.01.2017..
 */
class Adapter extends RecyclerView.Adapter<Holder> {

    private AppCompatActivity activity;
    private LayoutInflater inflater;
    private ArrayList<Linija> arrayList = Linija.generateDummyData();


    Adapter(AppCompatActivity activity) {
        this.activity = activity;
        inflater = activity.getLayoutInflater();

    }

    
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row_main, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final Linija linija = arrayList.get(position);
        holder.imageView.setImageResource(linija.getImageView());
        holder.tv.setText(linija.getOdLinija());
        holder.tv2.setText(linija.getDoLinija());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(activity, Activity2.class);
                intent.putStringArrayListExtra("workday", WebScrapper.scrapWorkDayRowToArrayList(activity, linija.getURL(), linija.getNumberOfRowsToScrap()));
                intent.putStringArrayListExtra("saturday", WebScrapper.scrapSaturdayRowToArrayList(activity, linija.getURL(), linija.getNumberOfRowsToScrap()));
                intent.putStringArrayListExtra("sunday", WebScrapper.scrapSundayRowToArrayList(activity, linija.getURL(), linija.getNumberOfRowsToScrap()));
                activity.startActivity(intent);*/
                WebScrapper ws = new WebScrapper();
                       ws.scrapWorkDayRowToArrayList(activity, linija);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
