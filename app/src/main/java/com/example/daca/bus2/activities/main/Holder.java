package com.example.daca.bus2.activities.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.bus2.R;

/**
 * Created by Daca on 18.01.2017..
 */
class Holder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView tv;
    TextView tv2;

    Holder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.imageView_main);
        tv = (TextView) itemView.findViewById(R.id.textView_main);
        tv2 = (TextView) itemView.findViewById(R.id.textView2_main);
    }
}
