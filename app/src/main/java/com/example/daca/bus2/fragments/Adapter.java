package com.example.daca.bus2.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.daca.bus2.R;
import com.example.daca.bus2.activities.activity2.Activity2;
import com.example.daca.bus2.activities.activity2.CustomMessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Daca on 19.01.2017..
 */

class Adapter extends RecyclerView.Adapter<Holder> {

    private Activity2 activity2;
    private LayoutInflater inflater;
    private ArrayList<String> arrayList = new ArrayList<>();

    public Adapter(Activity2 activity2, ArrayList<String> casList) {
        this.activity2 = activity2;
        inflater = activity2.getLayoutInflater();
        arrayList = casList;
        EventBus.getDefault().register(this);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_activity2_custom_row, parent, false);
        return new Holder(view);
    }

    @Override
    @Subscribe
    public void onBindViewHolder(Holder holder, int position) {
        CustomMessageEvent event = new CustomMessageEvent();
        event.getCustomStringMessage();
        int id2 = position + 4;
        int id = activity2.getResources().getIdentifier("com.example.daca.bus2:drawable/b" + id2, null, null);
        holder.imageView.setImageResource(id);

        if (arrayList.get(position).contains("&nbsp;")){
            holder.tv.setText("");
        }else {
            holder.tv.setText(arrayList.get(position));
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
