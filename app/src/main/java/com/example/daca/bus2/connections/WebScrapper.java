package com.example.daca.bus2.connections;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.daca.bus2.activities.activity2.Activity2;
import com.example.daca.bus2.activities.activity2.CustomMessageEvent;
import com.example.daca.bus2.entities.Linija;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import io.reactivex.Observable;


/**
 * Created by Daca on 22.01.2017..
 */

public class WebScrapper {

    public void scrapWorkDayRowToArrayList(final Activity activity, final Linija linija){
        final Intent intent;
        final ArrayList<String> casListRadniDan = new ArrayList<>();
        final ArrayList<String> casListSubota = new ArrayList<>();
        final ArrayList<String> casListNedelja = new ArrayList<>();
        final ArrayList<String> casListRadniDan2 = new ArrayList<>();
        final ArrayList<String> casListSubota2 = new ArrayList<>();
        final ArrayList<String> casListNedelja2 = new ArrayList<>();

        intent = new Intent(activity, Activity2.class);

        RequestQueue queue1 = ConnectionManager.getInstance(activity);

        StringRequest request1 = new StringRequest(Request.Method.GET, linija.getURL1(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Document doc = Jsoup.parse(response);
                        Elements trElements = doc.getElementsByTag("tr");
                        CustomMessageEvent event1 = new CustomMessageEvent();

                        for (int i = 2; i < 21; i++){
                            casListRadniDan.add(trElements.get(i).childNode(3).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                            event1.setCustomStringMessage(trElements.get(i).childNode(3).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                            EventBus.getDefault().post(event1);
                            casListSubota.add(trElements.get(i).childNode(5).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                            casListNedelja.add(trElements.get(i).childNode(7).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                        }
                        CustomMessageEvent event = new CustomMessageEvent();
                        event.setCustomMessage1(casListRadniDan);
                        event.setCustomMessage2(casListSubota2);
                        event.setCustomMessage3(casListNedelja);
                        EventBus.getDefault().post(event);
                        intent.putExtra("direction1", linija.getOdLinija());
                        intent.putExtra("direction2", linija.getDoLinija());
                        intent.putStringArrayListExtra("workday1", casListRadniDan);
                        intent.putStringArrayListExtra("saturday1", casListSubota);
                        intent.putStringArrayListExtra("sunday1", casListNedelja);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        queue1.add(request1);

        RequestQueue queue2 = ConnectionManager.getInstance(activity);

        StringRequest request2 = new StringRequest(Request.Method.GET, linija.getURL2(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Document doc = Jsoup.parse(response);
                        Elements trElements = doc.getElementsByTag("tr");

                        for (int i = 2; i < 21; i++){
                            casListRadniDan2.add(trElements.get(i).childNode(3).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                            casListSubota2.add(trElements.get(i).childNode(5).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                            casListNedelja2.add(trElements.get(i).childNode(7).childNode(1).childNode(0).childNode(0).childNode(0).toString());
                        }

                        String[] strings = new String[casListRadniDan.size()];

                        for (int i = 0; i < casListRadniDan.size(); i++){
                            strings[i] = casListRadniDan.get(i);
                        }

                        intent.putStringArrayListExtra("workday2", casListRadniDan2);
                        intent.putStringArrayListExtra("saturday2", casListSubota2);
                        intent.putStringArrayListExtra("sunday2", casListNedelja2);
                        activity.startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        queue2.add(request2);
    }
}


