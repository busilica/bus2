package com.example.daca.bus2.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.daca.bus2.R;
import com.example.daca.bus2.activities.activity2.Activity2;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 19.01.2017..
 */

public class Fragment extends android.support.v4.app.Fragment {


   ArrayList<String> casList = new ArrayList<>();
    Adapter adapter;



    public Fragment newInstance(ArrayList<String> casList){
       this.casList = casList;
        Fragment fragment = new Fragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("casList", casList);
        fragment.setArguments(bundle);
        return fragment;



    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity2, container, false);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.recyclerView_radni_dan);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(manager);
        Bundle bundle = getArguments();

        Observer<ArrayList<String>> observer = new Observer<ArrayList<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<String> value) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        ArrayList<String> list = bundle.getStringArrayList("casList");
        Activity2 activity2 = (Activity2) getActivity();
        Adapter adapter = new Adapter(activity2, list);
        rv.setAdapter(adapter);
        return view;
    }
}
