package com.example.daca.bus2.entities;

import com.example.daca.bus2.R;

import java.util.ArrayList;

/**
 * Created by Daca on 18.01.2017..
 */

public class Linija {

    private int numberOfRowsToScrap;
    private String odLinija;
    private String doLinija;
    public static String URL1;
    public static String URL2;
    private int imageView;

    public Linija(int numberOfRowsToScrap, String odLinija, String doLinija, String URL1, String URL2, int imageView) {
        this.numberOfRowsToScrap = numberOfRowsToScrap;
        this.odLinija = odLinija;
        this.doLinija = doLinija;
        this.imageView = imageView;
        this.URL1 = URL1;
        this.URL2 = URL2;
    }

    public static ArrayList<Linija> generateDummyData(){
        ArrayList<Linija> destinacija;
        destinacija = new ArrayList<>();
        destinacija.add(new Linija(23, "Niška Banja",  "MINOVO NASELJE", "http://media.jgpnis.rs/2015/10/1-niskab-minovonaselje.htm", "http://media.jgpnis.rs/2015/10/1-minovo-niska.htm", R.drawable.b1));
        destinacija.add(new Linija(22, "Bubanj", "DONJA VREZINA", "http://media.jgpnis.rs/2015/10/l2.bubanj-dvrezina.htm", "http://media.jgpnis.rs/2015/10/l2.dvrezina-bubanj.htm", R.drawable.b2));
        destinacija.add(new Linija(23, "Suvi do - Brzi Brod", "NASELJE RATKO JOVIĆ", "http://media.jgpnis.rs/2015/10/3-%D0%9D%D0%B0%D1%81%D0%B5%D1%99%D0%B5-%D0%91%D1%80%D0%B7%D0%B8-%D0%91%D1%80%D0%BE%D0%B4-%D0%A1%D1%83%D0%B2%D0%B8-%D0%94%D0%BE-%D0%9D.%D0%A0.-%D0%88%D0%BE%D0%B2%D0%B8%D1%9B.htm",
                "http://media.jgpnis.rs/2015/10/3-%D0%9D.%D0%A0.-%D0%88%D0%BE%D0%B2%D0%B8%D1%9B-%D0%9D%D0%B0%D1%81%D0%B5%D1%99%D0%B5-%D0%91%D1%80%D0%B7%D0%B8-%D0%91%D1%80%D0%BE%D0%B4-%D0%A1%D1%83%D0%B2%D0%B8-%D0%94%D0%BE-2.htm" ,R.drawable.b3));
        destinacija.add(new Linija(21, "Čalije","BUBANJ", "http://media.jgpnis.rs/2015/10/l4.calije-bubanj.htm", "http://media.jgpnis.rs/2015/10/4-%D0%A7%D0%B0%D0%BB%D0%B8%D1%98%D0%B5-%D0%91%D1%83%D0%B1%D0%B0%D1%9A.jpg", R.drawable.b4));
        destinacija.add(new Linija(22, "Železnička Stanica", "SOMBORSKA", "http://media.jgpnis.rs/2015/10/l5.zstanica-somborska.htm", "http://media.jgpnis.rs/2015/10/l5.somborska-zstanica.htm", R.drawable.b5));
        destinacija.add(new Linija(21, "Železnička Stanica", "Duvanište - Skopska", "http://media.jgpnis.rs/2015/10/l6.zstanica-duv-sko.htm", "http://media.jgpnis.rs/2015/10/l6.sko-duv-zstanica.htm", R.drawable.b5));
        //destinacija.add(new Linija("Sarajevska", "KALAČ BRDO", "", R.drawable.b6));
        //destinacija.add(new Linija("KALAČ BRDO", "Gabrovačka reka", "", R.drawable.b7));
        //destinacija.add(new Linija("Gabrovačka reka", "NOVO GROBLJE", "", R.drawable.b8));
        //destinacija.add(new Linija("PASI POLJANA", "Gabrovačka reka", "", R.drawable.b9));
        destinacija.add(new Linija(23, "Mokranjčeva", "BRANKO BJEGOVIĆ", "http://media.jgpnis.rs/2015/10/l9.mokr-bbjegovic.htm", "http://media.jgpnis.rs/2015/10/l9.bbjegovic-mokr.htm", R.drawable.b9));
        destinacija.add(new Linija(22, "Naselje 9. maj", "ĆELE KULA", "http://media.jgpnis.rs/2015/10/o10.novos-ckula-1.htm", "http://media.jgpnis.rs/2015/10/10-%D0%9D%D0%B0%D1%81%D0%B5%D1%99%D0%B5-9.-%D0%BC%D0%B0%D1%98-%E2%80%93-%D0%8B%D0%B5%D0%BB%D0%B5-%D0%9A%D1%83%D0%BB%D0%B0.htm", R.drawable.b10));
        destinacija.add(new Linija(15, "Donji Komren", "NJEGOŠEVA", "http://media.jgpnis.rs/2015/10/l12.dkomren-njegoseva.htm", "http://media.jgpnis.rs/2015/10/l12.njegoseva-dkomren.htm", R.drawable.b12));
        destinacija.add(new Linija(21, "Bulevar Nemanjića - Trg Kralja Aleksandra", "DELIJSKI VIS", "http://media.jgpnis.rs/2015/10/l13.tka-dvis.htm", "http://media.jgpnis.rs/2015/10/l13.dvis-tka.htm", R.drawable.b13));
        //destinacija.add(new Linija("AERODROM B", "Železnička Stanica - A. Stanica - Aerodrom A", "", R.drawable.b34));
        //destinacija.add(new Linija("MRAMOR", "Trg Kralja Aleksandra", "", R.drawable.b36));


        return destinacija;
    }

    public int getNumberOfRowsToScrap() {
        return numberOfRowsToScrap;
    }

    public void setNumberOfRowsToScrap(int numberOfRowsToScrap) {
        this.numberOfRowsToScrap = numberOfRowsToScrap;
    }

    public String getOdLinija() {
        return odLinija;
    }

    public void setOdLinija(String odLinija) {
        this.odLinija = odLinija;
    }

    public String getDoLinija() {
        return doLinija;
    }

    public void setDoLinija(String doLinija) {
        this.doLinija = doLinija;
    }

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }

    public String getURL2() {
        return URL2;
    }

    public String getURL1() {
        return URL1;
    }
}
